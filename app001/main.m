//
//  main.m
//  app001
//
//  Created by William Muro on 7/29/13.
//  Copyright (c) 2013 MAU. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "APP001AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([APP001AppDelegate class]));
    }
}
