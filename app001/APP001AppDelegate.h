//
//  APP001AppDelegate.h
//  app001
//
//  Created by William Muro on 7/29/13.
//  Copyright (c) 2013 MAU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APP001AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
