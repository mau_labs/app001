//
//  APP001_Playlists_ViewController.m
//  app001
//
//  Created by William Muro on 8/10/13.
//  Copyright (c) 2013 MAU. All rights reserved.
//

#import "APP001_Playlists_ViewController.h"

@interface APP001_Playlists_ViewController ()

@end

@implementation APP001_Playlists_ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
