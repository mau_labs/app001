//
//  APP001_P3_Cell.m
//  app001
//
//  Created by William Muro on 8/11/13.
//  Copyright (c) 2013 MAU. All rights reserved.
//

#import "APP001_P3_Cell.h"

@implementation APP001_P3_Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
