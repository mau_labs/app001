

//////////////////////////////////////////////PANTALLA CANCIONES - P1///////////////////////////////////////////////

//NOMBRE DE LA VISTA

P1_celda_cancion

//MEDIDAS

//Ancho de la celda:
#define P1_W_CELDA_CANCION 320

//Alto de la celda
#define P1_H_CELDA_CANCION 95

//Distancia DX desde el punto superior izquierdo al origen
#define P1_DX_CELDA_CANCION 0

//Distancia DY desde el punto superior izquierdo al origen
#define P1_DY_CELDA_CANCION 90

//Centro de la celda X
#define P1_CX_CELDA_CANCION 160

//Centro de la celda Y
#define P1_CY_CELDA_CANCION 137

//COLOR
#define P1_COLOR_FONDO_CELDA_CANCION [UIColor clearColor];

//Declaración:
//Declaramos la label
UIView *P1_celda_cancion;

//La asignamos como propiedad del controlador
@property(nonatomic,retain) UIView *P1_celda_cancion;

//Inicialización
@synthesize P1_celda_cancion;

//Configuración

UIView * P1_celda_cancion = [[UIView alloc] initWithFrame:CGRectMake(P1_DX_CELDA_CANCION,P1_DY_CELDA_CANCION,P1_W_CELDA_CANCION, P1_H_CELDA_CANCION)];
P1_celda_cancion.center = CGPointMake(P1_CX_CELDA_CANCION,P1_CY_CELDA_CANCION Y);
P1_celda_cancion.backgroundColor = P1_COLOR_FONDO_CELDA_CANCION;
P1_celda_cancion.tag = 0;


[self.view addSubview:P1_celda_cancion];





P1_nombre_cancion

#define P1_FUENTE_NOMBRE_CANCION [UIFont fontWithName:@"SourceSansPro-semibold" size:(40)]

#define P1_TXT_NOMBRE_CANCION NSLocalizedString(@"P1_nombre_cancion_KEY", @"")

"P1_nombre_cancion_KEY" = "el texto en español"

"P1_nombre_cancion_KEY" = "el texto en inglés"


#define P1_W_CELDA 320
#define P1_H_CELDA 95


#define P1_W_NOMBRE_CANCION 167
#define P1_H_NOMBRE_CANCION 19

#define P1_DX_NOMBRE_CANCION 100
#define P1_DY_NOMBRE_CANCION 15

#define P1_CX_NOMBRE_CANCION 183
#define P1_CY_NOMBRE_CANCION 24
#define P1_COLOR_NOMBRE_CANCION [UIColor colorWithRed:0.992f green:0.992f blue:0.992f alpha:1.0f]

COLOR_FONDO_NOMBRE_CANCION [UIColor clearColor];

#define P1_ALINEACIONT_NOMBRE_CANCION NSTextAlignmentCenter
#define P1_ALINEACIONC_NOMBRE_CANCION UIViewContentModeCenter

UILabel *P1_nombre_cancion;

@property(nonatomic,retain) UILabel *P1_nombre_cancion;
@synthesize P1_nombre_cancion

P1_nombre_cancion = [[UILabel alloc] initWithFrame:CGRectMake(P1_DX_NOMBRE_CANCION, P1_DY_NOMBRE_CANCION, P1_W_NOMBRE_CANCION, P1_H_NOMBRE_CANCION)];
P1_nombre_cancion.adjustsFontSizeToFitWidth = NO;
P1_nombre_cancion.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
P1_nombre_cancion.clearsContextBeforeDrawing = YES;
P1_nombre_cancion.clipsToBounds = YES;
P1_nombre_cancion.contentMode = P1_ALINEACIONC_NOMBRE_CANCION;
P1_nombre_cancion.enabled = YES;
P1_nombre_cancion.center = CGPointMake(P1_CX_NOMBRE_CANCION, P1_CY_NOMBRE_CANCION);
P1_nombre_cancion.hidden = NO;
	
P1_nombre_cancion.lineBreakMode = NSLineBreakByWordWrapping;
P1_nombre_cancion.numberOfLines = 2;
P1_nombre_cancion.opaque = YES;
P1_nombre_cancion.tag = 0;
P1_nombre_cancion.text = P1_TXT_NOMBRE_CANCION;
P1_nombre_cancion.textAlignment = P1_ALINEACIONT_NOMBRE_CANCION;
P1_nombre_cancion.backgroundColor = COLOR_TRASPARENTE;
P1_nombre_cancion.textColor = P1_COLOR_NOMBRE_CANCION;
[P1_nombre_cancion setFont:P1_FUENTE_NOMBRE_CANCION];

[self.view addSubview:P1_nombre_cancion];
