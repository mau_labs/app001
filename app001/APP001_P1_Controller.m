//
//  APP001_P1_Controller.m
//  app001
//
//  Created by William Muro on 7/29/13.
//  Copyright (c) 2013 MAU. All rights reserved.
//

#import "APP001_P1_Controller.h"

@interface APP001_P1_Controller ()

@end

@implementation APP001_P1_Controller

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
