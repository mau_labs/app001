//
//  APP001_P1_Controller.h
//  app001
//
//  Created by William Muro on 7/29/13.
//  Copyright (c) 2013 MAU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APP001_P1_Controller : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    UILabel *P1_nombre_cancion;
    
}
@property(nonatomic,retain) UILabel *P1_nombre_cancion;

@end
