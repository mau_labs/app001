
#define P1_FUENTE_NOMBRE_CANCION [UIFont fontWithName:@"SourceSansPro-semibold" size:(40)]

#define P1_TXT_NOMBRE_CANCION NSLocalizedString(@"P1_nombre_cancion_KEY", @"")

//"P1_nombre_cancion_KEY" = "el texto en espa�ol"

//"P1_nombre_cancion_KEY" = "el texto en ingl�s"

#define P1_W_CELDA 320
#define P1_H_CELDA 95

#define P1_W_NOMBRE_CANCION 167
#define P1_H_NOMBRE_CANCION 19

#define P1_DX_NOMBRE_CANCION 100
#define P1_DY_NOMBRE_CANCION 15

#define P1_CX_NOMBRE_CANCION 160
#define P1_CY_NOMBRE_CANCION 24

#define P1_COLOR_NOMBRE_CANCION [UIColor colorWithRed:0.992f green:0.992f blue:0.992f alpha:1.0f]

#define P1_ALINEACIONT_NOMBRE_CANCION NSTextAlignmentCenter
#define P1_ALINEACIONC_NOMBRE_CANCION UIViewContentModeCenter
